#pragma once

#define VERSION_MAJOR 1
#define VERSION_MINOR 2

class Program
{
private:
	struct character_t*** screenbuffer;
	int chat_socket;
	int line_at;
	int row_at;
	char sbuf[1024];

public:
	Program();
	~Program();
	void run();
	int raw(bool isSSL, const char* fmt, ...);
	void append_screenbuffer(unsigned char* buffer);
	void clear_screenbuffer();
	void scroll_up();
	SSL_CTX *ctx;
	SSL* ssl;
};

