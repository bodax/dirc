CXXFLAGS=-std=c++17

DEPS = Program.cpp

OBJ = Program.o

%.o: %.cpp $(DEPS)
	$(CXX) -c -o $@ $< $(CXXFLAGS) -I../odoors/ -g
%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS) -I../odoors/ -g

dirc: $(OBJ)
	$(CXX) -o $@ $^ $(LDFLAGS) ../odoors/libs-Linux/libODoors.a -lcrypto -lssl -lpthread -ldl -lstdc++fs -g

.PHONY: clean

clean:
	rm -f $(OBJ) dirc
