#if defined(_MSC_VER) || defined(WIN32)
#define WIN32_LEAN_AND_MEAN 1
#endif

#include <OpenDoor.h>
#if defined(_MSC_VER) || defined(WIN32)
#include <winsock2.h>
#include <ws2ipdef.h>
#include <ws2tcpip.h>
#include <windows.h>
#include <wincrypt.h>
#else
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#define _strdup strdup
#define _stricmp strcasecmp
#define strncat_s strncat
#define strncpy_s strncpy
#define _strnicmp strncasecmp
#define _w_inet_pton inet_pton
#define _w_inet_ntop inet_ntop
#endif
#include <cctype>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cstdarg>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/x509v3.h>
#include <openssl/x509_vfy.h>
#include "INIReader.h"
#include "Program.h"

char *uhandle;
bool ampm = false;

#if defined(_MSC_VER) || defined(WIN32)
int _w_inet_pton(int af, const char *src, void *dst)
{
  struct sockaddr_storage ss;
  int size = sizeof(ss);
  char src_copy[INET6_ADDRSTRLEN+1];

  ZeroMemory(&ss, sizeof(ss));
  /* stupid non-const API */
  strncpy (src_copy, src, INET6_ADDRSTRLEN+1);
  src_copy[INET6_ADDRSTRLEN] = 0;

  if (WSAStringToAddressA(src_copy, af, NULL, (struct sockaddr *)&ss, &size) == 0) {
    switch(af) {
      case AF_INET:
    *(struct in_addr *)dst = ((struct sockaddr_in *)&ss)->sin_addr;
    return 1;
      case AF_INET6:
    *(struct in6_addr *)dst = ((struct sockaddr_in6 *)&ss)->sin6_addr;
    return 1;
    }
  }
  return 0;
}

const char *_w_inet_ntop(int af, const void *src, char *dst, socklen_t size)
{
  struct sockaddr_storage ss;
  unsigned long s = size;

  ZeroMemory(&ss, sizeof(ss));
  ss.ss_family = af;

  switch(af) {
    case AF_INET:
      ((struct sockaddr_in *)&ss)->sin_addr = *(struct in_addr *)src;
      break;
    case AF_INET6:
      ((struct sockaddr_in6 *)&ss)->sin6_addr = *(struct in6_addr *)src;
      break;
    default:
      return NULL;
  }
  /* cannot direclty use &size because of strict aliasing rules */
  return (WSAAddressToStringA((struct sockaddr *)&ss, sizeof(ss), NULL, dst, &s) == 0)?
          dst : NULL;
}

#endif

char* stristr(const char* ch1, const char* ch2)
{
	char* chN1, * chN2;
	char* chNdx;
	char* chRet = NULL;

	chN1 = _strdup(ch1);
	chN2 = _strdup(ch2);
	if (chN1 && chN2)
	{
		chNdx = chN1;
		while (*chNdx)
		{
			*chNdx = (char)tolower(*chNdx);
			chNdx++;
		}
		chNdx = chN2;
		while (*chNdx)
		{
			*chNdx = (char)tolower(*chNdx);
			chNdx++;
		}
		chNdx = strstr(chN1, chN2);
		if (chNdx)
			chRet = (char *)ch1 + (chNdx - chN1);
	}
	free(chN1);
	free(chN2);
	return chRet;
}

struct character_t {
	char c;
	int color;
};

static char * replace_spaces(const char* in) {

    char *out = strdup(in);

    for (int i=0; i< strlen(out); i++) {
        if (!isalnum(out[i])) {
            if (out[i] == '\r' || out[i] == '\n') {
                out[i] = '\0';
                return out;
            }
            out[i] = '_';
        }
    }

    return out;
}

int hostname_to_ip(const char* hostname, char* ip) {
	struct addrinfo hints, * res, * p;
	int status;
	struct sockaddr_in* ipv4;

	memset(&hints, 0, sizeof(hints));

	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	if ((status = getaddrinfo(hostname, NULL, &hints, &res)) != 0) {
		od_printf("Get addr info failed %d\n", status);
		return 1;
	}

	for (p = res; p != NULL; p = p->ai_next) {
		if (p->ai_family == AF_INET) {
			ipv4 = (struct sockaddr_in*)p->ai_addr;
			_w_inet_ntop(p->ai_family, &(ipv4->sin_addr), ip, INET_ADDRSTRLEN);
			freeaddrinfo(res);
			return 0;
		}
	}
	freeaddrinfo(res);
	od_printf("No ipv4 addr");
	return 1;
}

int chat_connect_ipv4(const char *server, uint16_t port, int* socketp) {
	struct sockaddr_in servaddr;
	int chat_socket;
	char buffer[513];
	u_long iMode = 1;
	memset(&servaddr, 0, sizeof(struct sockaddr_in));
	if (_w_inet_pton(AF_INET, server, &servaddr.sin_addr) != 1) {
		if (hostname_to_ip(server, buffer)) {
			od_printf("Hostname to IP failed for %s\n", server);
			return 0;
		}
		if (!_w_inet_pton(AF_INET, buffer, &servaddr.sin_addr)) {
			od_printf("Inet_pton failed for %s\n", buffer);
			return 0;
		}
	}
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(port);
	if ((chat_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		od_printf("Create socket failed\n");
		return 0;
	}


	if (connect(chat_socket, (struct sockaddr*) & servaddr, sizeof(servaddr)) < 0) {
		od_printf("Connect socket failed\n");
		return 0;
	}
#if defined(_MSC_VER) || defined(WIN32)
	ioctlsocket(chat_socket, FIONBIO, &iMode);
#else
	ioctl(chat_socket, FIONBIO, &iMode);
#endif
	*socketp = chat_socket;
	return 1;
}

int Program::raw(bool isSSL, const char* fmt, ...) {
	memset(sbuf, 0, 1024);
	va_list ap;
	va_start(ap, fmt);
	vsnprintf(sbuf, 1023, fmt, ap);
	va_end(ap);
    if (isSSL) {
		int ret;

		ret = SSL_write(ssl, sbuf, strlen(sbuf));
		return ret;
    } else {
        return send(chat_socket, sbuf, strlen(sbuf), 0);
    }
}

Program::Program()
{
	screenbuffer = NULL;
	chat_socket = -1;
	line_at = 1;
	row_at = 1;
}


Program::~Program()
{
}

void Program::scroll_up() {
	int y;
	int x;
	int color;

	for (y = 1; y < 23; y++) {
		for (x = 0; x < 80; x++) {
			memcpy(screenbuffer[y - 1][x], screenbuffer[y][x], sizeof(struct character_t));
			color = screenbuffer[y][x]->color;
		}
	}
	for (x = 0; x < 80; x++) {
		screenbuffer[22][x]->c = '\0';
		screenbuffer[22][x]->color = color;
	}
	row_at = 0;
}

void Program::clear_screenbuffer() {
	for (int x = 0; x < 80; x++) {
		for (int y = 0; y < 23; y++) {
			screenbuffer[y][x]->c = '\0';
			screenbuffer[y][x]->color = 7;
		}
	}
	line_at = 0;
	row_at = 0;
}

void Program::append_screenbuffer(unsigned char* buffer) {
	int z;
	int i;
	int last_space = 0;
	int last_pos = 0;
	int curr_color = 7;

    bool gotcolour = false;

    unsigned char *buffer2 = (unsigned char *)strdup((char *)buffer);

    i = 0;

    buffer[0] = '\0';

    for (z = 0; z < strlen((char *)buffer2);z++) {
        if (buffer2[z] == 2 || buffer2[z] == 20 || buffer2[z] == 9 || buffer2[z] == 18) {
            z++;
            continue;
        }

        if (buffer2[z] == 15) {
            gotcolour = false;
            z++;
            continue;
        }

        if (buffer2[z] == 3) {
            if (gotcolour) {
                z++;
                gotcolour = false;
                continue;
            } else {
                z++;
                if (z >= strlen((char *)buffer2)) break;
                if (isdigit(buffer2[z])) z++;
                if (z >= strlen((char *)buffer2)) break;
                if (isdigit(buffer2[z])) z++;
                if (z >= strlen((char *)buffer2)) break;
                if (buffer2[z] == ',') {
                    z++;
                    if (isdigit(buffer2[z])) z++;
                    if (z >= strlen((char *)buffer2)) break;
                    if (isdigit(buffer2[z])) z++;
                    if (z >= strlen((char *)buffer2)) break;
                }
                gotcolour = true;
            }
        }
        buffer[i++] = buffer2[z];
        buffer[i] = '\0';
    }

    free(buffer2);

	for (z = 0; z < strlen((char *)buffer); z++) {
		if (buffer[z] == '|') {
			z++;
			if ((buffer[z] - '0' <= 2 && buffer[z] - '0' >= 0) && (buffer[z + 1] - '0' <= 9 && buffer[z + 1] - '0' >= 0)) {
				curr_color = (buffer[z] - '0') * 10 + (buffer[z + 1] - '0');
				z += 2;
			}
			else {
				z--;
			}
		}

		if (row_at == 79) {
			if (line_at == 22) {
				if (last_space > 0) {
					for (i = last_space; i <= row_at; i++) {
						screenbuffer[line_at][i]->c = '\0';
						screenbuffer[line_at][i]->color = curr_color;
					}
				} else {
					last_pos = z;
				}
				scroll_up();
				row_at = 0;
				for (i = last_pos + 1; i < z; i++) {
					if (buffer[i] == '|') {
						i++;
						if ((buffer[i] - '0' <= 2 && buffer[i] - '0' >= 0) && (buffer[i + 1] - '0' <= 9 && buffer[i + 1] - '0' >= 0)) {
							curr_color = (buffer[i] - '0') * 10 + (buffer[i + 1] - '0');
							i += 2;
						}
						else {
							i--;
						}
					}
					if (i < strlen((char *)buffer)) {
						screenbuffer[line_at][row_at]->c = buffer[i];
						screenbuffer[line_at][row_at++]->color = curr_color;
					}
				}
				last_space = 0;
				last_pos = 0;
			}
			else {
				if (last_space > 0) {
					for (i = last_space; i <= row_at; i++) {
						screenbuffer[line_at][i]->c = '\0';
						screenbuffer[line_at][i]->color = curr_color;
					}
				} else {
					last_pos = z;
				}
				line_at++;
				row_at = 0;
				for (i = last_pos + 1; i < z; i++) {
					if (buffer[i] == '|') {
						i++;
						if ((buffer[i] - '0' <= 2 && buffer[i] - '0' >= 0) && (buffer[i + 1] - '0' <= 9 && buffer[i + 1] - '0' >= 0)) {
							curr_color = (buffer[i] - '0') * 10 + (buffer[i + 1] - '0');
							i += 2;
						}
						else {
							i--;
						}
					}
					if (i < strlen((char *)buffer)) {
						screenbuffer[line_at][row_at]->c = buffer[i];
						screenbuffer[line_at][row_at++]->color = curr_color;
					}
				}
				last_space = 0;
			}
		}

		if (buffer[z] == ' ') {
			last_space = row_at;
			last_pos = z;
		}

		if (z < strlen((char *)buffer)) {
			screenbuffer[line_at][row_at]->c = buffer[z];
			screenbuffer[line_at][row_at]->color = curr_color;
			row_at++;
			screenbuffer[line_at][row_at]->c = '\0';
			screenbuffer[line_at][row_at]->color = curr_color;
		}
	}
	if (line_at == 22) {
		scroll_up();
	}
	if (line_at < 22) {
		line_at++;
	}

	row_at = 0;
}

SSL_CTX* InitCTX(void)
{
	SSL_METHOD* method;
	SSL_CTX* ctx;
	OpenSSL_add_all_algorithms();  /* Load cryptos, et.al. */
	SSL_load_error_strings();   /* Bring in and register error messages */
	method = (SSL_METHOD *)TLS_method();  /* Create new client-method instance */
	ctx = SSL_CTX_new(method);   /* Create new context */
	if (ctx == NULL)
	{
		od_exit(-1, FALSE);
	}
	return ctx;
}

#ifdef _MSC_VER
static int cryptoapi_ca_cert(SSL_CTX *ssl_ctx, const char *store_name) {
    HCERTSTORE cs;
    PCCERT_CONTEXT ctx = NULL;
    X509 *cert;
	X509_STORE *store;
    char buf[128];

    store = X509_STORE_new();

     cs = CertOpenSystemStoreA(0, store_name);
     if (cs == NULL) {
	//	printf("[-] CryptoAPI: failed to open system cert store '%s': error=%d\n", store_name,(int) GetLastError());
		return -1;
	}

	while((ctx = CertEnumCertificatesInStore(cs, ctx))) 
    {
		cert = d2i_X509(NULL, (const unsigned char **)&ctx->pbCertEncoded,ctx->cbCertEncoded);
		if (cert == NULL) 
		{
        	// printf("[-] CryptoAPI: Could not process X509 DER encoding for CA cert\n");
			continue;
		}

		X509_NAME_oneline(X509_get_subject_name(cert), buf, sizeof(buf));
		//printf("[+] OpenSSL: Loaded CA certificate for system certificate store:\n subject='%s'\n\n", buf);

		if (!X509_STORE_add_cert(store, cert))  {
			//printf("[-] Failed to add ca_cert to OpenSSL certificate store");
		}
		X509_free(cert);
	}

    if (!CertCloseStore(cs, 0))  {
    	//	printf("[-] failed to close system cert store '%s': error=%d", store_name,(int) GetLastError());
	}
	SSL_CTX_set_cert_store(ssl_ctx, store);
    return 0;
}
#endif

void Program::run() {
	char inputbuffer[256];
	int inputbuffer_at = 0;
	int len;
	char c;
	char buffer2[1024];
	char outputbuffer[1024];
	char readbuffer[1024];
	char message[1024];
	char partmessage[1024];
	int do_update = 1;
	int i;
	int z;
	int y;
	int last_color = 7;
	int chat_connected = 0;
	time_t msgtime;
	struct tm msgtime_tm;
	bool sent_login = false;
	long secure = 0;
	const char *SSL_str[] = {"`bright yellow`ERR`bright black`", "`bright green`SSL`bright black`", "`bright red`CLR`bright black`"};

	time_t connected_at;

    bool usessl = false;

    memset(inputbuffer, 0, 256);

    bool sentjoin = false;


    INIReader inir(od_control_get()->od_config_filename);

    ampm = inir.GetBoolean("Main", "ampm", false);


	if (strlen(od_control_get()->user_handle) > 0) {
		uhandle = replace_spaces(od_control_get()->user_handle);
	}
	else if (strlen(od_control_get()->user_name) > 0) {
		uhandle = replace_spaces(od_control_get()->user_name);
	}
	else {
		uhandle = strdup("NoName");
	}

	if (inir.GetBoolean("Main", "notimeout", false)) {
		od_control_get()->od_disable_inactivity = TRUE;
	}

	row_at = 0;
	line_at = 0;
	od_clr_scr();
	od_set_cursor(23, 1);
	od_printf("`bright black`-------------------------------------------------------------------------------");
	od_set_cursor(23, 1);
	od_printf("`bright black`[`bright cyan`%s`bright black`][%s]", uhandle, (secure == 0 ? SSL_str[1]: SSL_str[0]));
	od_set_cursor(24, 1);
	od_printf("`bright black`[`bright green`%s`bright black`]", inir.Get("Main", "Room", "#talisman").c_str());
	od_set_cursor(24, strlen(inir.Get("Main", "Room", "#talisman").c_str()) + 3);
	od_set_color(D_GREY, D_BLACK);

    usessl = inir.GetBoolean("Main", "Use SSL", false);

    if (usessl) {
        SSL_library_init();
        ctx = InitCTX();
    }

	chat_connected = chat_connect_ipv4(inir.Get("Main", "Server", "localhost").c_str(), inir.GetInteger("Main", "Port", 6667), &chat_socket);

	if (!chat_connected) {
		od_printf("Chat failed to connect!\n");
		return;
	}

	if (usessl) {

#if _MSC_VER
        if (cryptoapi_ca_cert(ctx, "ROOT") == 0) {
            secure = 0;
        }
#else
        if (!SSL_CTX_load_verify_locations(ctx, NULL, "/etc/ssl/certs/")) {
            secure = -1;
        }
        if (!SSL_CTX_set_default_verify_paths(ctx)) {
            secure = -1;
        }
#endif

        SSL_CTX_set_verify(ctx, SSL_VERIFY_NONE, NULL);

        ssl = SSL_new(ctx);

        SSL_set_hostflags(ssl, X509_CHECK_FLAG_NO_PARTIAL_WILDCARDS);
        if (!SSL_set1_host(ssl, inir.Get("Main", "Server", "localhost").c_str())) {
            /* handle error */
            od_exit(-1, FALSE);
        }

        SSL_set_verify(ssl, SSL_VERIFY_NONE, NULL);

        SSL_set_fd(ssl, chat_socket);
		int ret;
		while ((ret = SSL_connect(ssl)) == -1) {
			fd_set fds;
			FD_ZERO(&fds);
			FD_SET(chat_socket, &fds);
			switch (SSL_get_error(ssl, ret))
			{
				case SSL_ERROR_WANT_READ:
					fprintf(stderr, "WANT_READ");
					select(chat_socket + 1, &fds, NULL, NULL, NULL);
					break;
				case SSL_ERROR_WANT_WRITE:
					fprintf(stderr, "WANT_WRITE");
					select(chat_socket + 1, NULL, &fds, NULL, NULL);
					break;
				default:
					fprintf(stderr, "ERROR");
		#ifdef _MSC_VER
					closesocket(chat_socket);
		#else
					close(chat_socket);
		#endif
					SSL_shutdown(ssl);
					SSL_free(ssl);
					SSL_CTX_free(ctx);
					od_exit(-1, FALSE);
					return;
			}
		}
        if (ret == 0) {
#ifdef _MSC_VER
            closesocket(chat_socket);
#else
            close(chat_socket);
#endif
            SSL_shutdown(ssl);
            SSL_free(ssl);
            SSL_CTX_free(ctx);
            od_exit(-1, FALSE);
        }
    }

	
	screenbuffer = (struct character_t***)malloc(sizeof(struct character_t**) * 23);
	for (i = 0; i < 23; i++) {
		screenbuffer[i] = (struct character_t**)malloc(sizeof(struct character_t*) * 80);
		for (z = 0; z < 80; z++) {
			screenbuffer[i][z] = (struct character_t*)malloc(sizeof(struct character_t));
			screenbuffer[i][z]->c = '\0';
			screenbuffer[i][z]->color = 7;
		}
	}
	memset(partmessage, 0, 1024);

	while (1) {
		tODInputEvent ev;

        c = 0;
		
		if (od_get_input(&ev, 10, 0)) {
			if (ev.EventType == EVENT_CHARACTER) {
                c = ev.chKeyPress;
            }
        }
		if (c == 0) {
			do_update = 0;
		} else {
			if (c == '\r' || strlen(inputbuffer) >= 255) {
				msgtime = time(NULL);
#if defined(WIN32) || defined(_MSC_VER)
				localtime_s(&msgtime_tm, &msgtime);
#else
				localtime_r(&msgtime, &msgtime_tm);
#endif
				if (inputbuffer[0] == '/') {
					if (_strnicmp(&inputbuffer[1], "quit", 4) == 0) {
                        if (strlen(inputbuffer) > 6) {
                            raw(usessl, "QUIT :%s\n", &inputbuffer[6]);
                        } else {
                            raw(usessl, "QUIT :Leaving!\n");
                        }
                    } else if (_stricmp(&inputbuffer[1], "q") == 0) {
                        raw(usessl, "QUIT :Leaving!\n");
                    } else if (_stricmp(&inputbuffer[1], "help") == 0 || _stricmp(&inputbuffer[1], "?") == 0) {
                        snprintf(buffer2, sizeof buffer2, " ");
						append_screenbuffer((unsigned char*)buffer2);
						snprintf(buffer2, sizeof buffer2, "|10--- |15HELP |10------------------------------------------------------------|07");
						append_screenbuffer((unsigned char*)buffer2);
						snprintf(buffer2, sizeof buffer2, "|10/?          /help         |15Show this Help|07");
						append_screenbuffer((unsigned char*)buffer2);
						snprintf(buffer2, sizeof buffer2, "|10            /12hr /24hr   |15Switch timestamp style|07");
						append_screenbuffer((unsigned char*)buffer2);
						snprintf(buffer2, sizeof buffer2, "|10/c          /cls          |15Clear the screen|07");
						append_screenbuffer((unsigned char*)buffer2);
						snprintf(buffer2, sizeof buffer2, "|10/s          /status       |15Your status|07");
						append_screenbuffer((unsigned char*)buffer2);
						snprintf(buffer2, sizeof buffer2, "|10/v          /version      |15Client version & SSL info|07");
						append_screenbuffer((unsigned char*)buffer2);
						snprintf(buffer2, sizeof buffer2, "|10            /msg target   |15Send a Private Message to \"target\"|07");
						append_screenbuffer((unsigned char*)buffer2);
						snprintf(buffer2, sizeof buffer2, "|10            /me action    |15Perform an \"action\"|07");
						append_screenbuffer((unsigned char*)buffer2);
						snprintf(buffer2, sizeof buffer2, "|10            /nick foo     |15Change your nick to \"foo\"|07");
						append_screenbuffer((unsigned char*)buffer2);
						snprintf(buffer2, sizeof buffer2, "|10            /names        |15List who is in the channel|07");
						append_screenbuffer((unsigned char*)buffer2);
						snprintf(buffer2, sizeof buffer2, "|10/q          /quit         |15Quit to BBS|07");
						append_screenbuffer((unsigned char*)buffer2);
						snprintf(buffer2, sizeof buffer2, "|10---------------------------------------------------------------------|07");
						append_screenbuffer((unsigned char*)buffer2);
						snprintf(buffer2, sizeof buffer2, " ");
						append_screenbuffer((unsigned char*)buffer2);
                        do_update = 1;
					} else if (_stricmp(&inputbuffer[1], "12hr") == 0) {
						ampm = true;
						do_update = 2;
					}
					else if (_stricmp(&inputbuffer[1], "24hr") == 0) {
						ampm = false;
						do_update = 2;
					} else if (_stricmp(&inputbuffer[1], "cls") == 0 || _stricmp(&inputbuffer[1], "c") == 0) {
						clear_screenbuffer();
						do_update = 1;
                    } else if (_strnicmp(&inputbuffer[1], "me", 2) == 0) {
                        raw(usessl, "PRIVMSG %s :\001ACTION %s\001\n",  inir.Get("Main", "Room", "#talisman").c_str(), &inputbuffer[4]);
						if (ampm) {
							int hour = msgtime_tm.tm_hour;
							if (hour == 0 || hour <= 12) {
								if (hour == 0) {
									hour = 12;
								}
								snprintf(buffer2, sizeof buffer2, "|08%02d:%02dam |07*|11%s |07%s", hour, msgtime_tm.tm_min, uhandle, &inputbuffer[4]);
							} else {
                                if (hour != 12) {
                                    hour -= 12;
                                }
                                snprintf(buffer2, sizeof buffer2, "|08%02d:%02dpm |07*|11%s |07%s", hour, msgtime_tm.tm_min, uhandle, &inputbuffer[4]);
							}
						} else {
							snprintf(buffer2, sizeof buffer2, "|08%02d:%02dam |07*|11%s |07%s", msgtime_tm.tm_hour, msgtime_tm.tm_min, uhandle, &inputbuffer[4]);
						}
						append_screenbuffer((unsigned char*)buffer2);
                        do_update = 1;
					} else if (_stricmp(&inputbuffer[1], "status") == 0 ||  _stricmp(&inputbuffer[1], "s") == 0) {
						time_t now = time(NULL);

						int hours = (now - connected_at) / 60 / 60;
						int minutes = (now - connected_at) / 60 % 60;
						snprintf(buffer2, sizeof buffer2, " ");
						append_screenbuffer((unsigned char*)buffer2);
						snprintf(buffer2, sizeof buffer2, "|10You are in the room |15%s|10.", inir.Get("Main", "Room", "#talisman").c_str());
						append_screenbuffer((unsigned char*)buffer2);
						snprintf(buffer2, sizeof buffer2, "|10You have been connected for |15%d hours|10, |15%d minutes|10.", hours, minutes);
						append_screenbuffer((unsigned char*)buffer2);
						snprintf(buffer2, sizeof buffer2, " ");
						append_screenbuffer((unsigned char*)buffer2);
						do_update = 1;
					} else if (_stricmp(&inputbuffer[1], "version") == 0 || _stricmp(&inputbuffer[1], "v") == 0) {
						snprintf(buffer2, sizeof buffer2, " ");
						append_screenbuffer((unsigned char*)buffer2);
						snprintf(buffer2, sizeof buffer2, "|09dIRC Client Ver %d.%d", VERSION_MAJOR, VERSION_MINOR);
						append_screenbuffer((unsigned char*)buffer2);
                        if (usessl) {
                            X509 *cert;
                            char line[256];

                            cert = SSL_get_peer_certificate(ssl);
                            if (cert != NULL) {
                                X509_NAME_oneline(X509_get_subject_name(cert), line, 256);
                                snprintf(buffer2, sizeof buffer2, "|14Cert Subject: %s", line);
                                append_screenbuffer((unsigned char*)buffer2);
                                X509_NAME_oneline(X509_get_issuer_name(cert), line, 256);
                                snprintf(buffer2, sizeof buffer2, " |14Cert Issuer: %s", line);
                                append_screenbuffer((unsigned char*)buffer2);
                                X509_free(cert);
                                snprintf(buffer2, sizeof buffer2, " |14Cert Status: %s", X509_verify_cert_error_string(secure));
                                append_screenbuffer((unsigned char*)buffer2);
                            } else {
                                snprintf(buffer2, sizeof buffer2, "|14Cert Error");
                                append_screenbuffer((unsigned char*)buffer2);
                            }
                        }
						snprintf(buffer2, sizeof buffer2, " ");
						append_screenbuffer((unsigned char*)buffer2);
						memset(inputbuffer, 0, 256);
						inputbuffer_at = 0;
						do_update = 1;
                    } else if (_stricmp(&inputbuffer[1], "names") == 0) {
                        raw(usessl, "NAMES %s\n",inir.Get("Main", "Room", "#talisman").c_str());
					} else if (_strnicmp(&inputbuffer[1], "msg", 3) == 0 && strlen(inputbuffer) > 5) {
                        char *msgto = &inputbuffer[5];
                        char *msgb = NULL;
                        for (int l = 0 ; l < strlen(msgto); l++) {
                            if (msgto[l] == ' ') {
                                msgb = &msgto[l+1];
                                msgto[l] = '\0';
                                break;
                            }
                        }
                        raw(usessl, "PRIVMSG %s :%s\n", msgto, msgb);

						if (ampm) {
							int hour = msgtime_tm.tm_hour;
							if (hour == 0 || hour < 12) {
								if (hour == 0) {
									hour = 12;
								}
								snprintf(buffer2, sizeof buffer2, "|08%02d:%02dam [|11%s|08->|10%s|08] |02%s|07", hour, msgtime_tm.tm_min, uhandle, msgto, msgb);
							} else {
                                if (hour != 12) {
                                    hour -= 12;
                                }
								snprintf(buffer2, sizeof buffer2, "|08%02d:%02dpm [|11%s|08->|10%s|08] |02%s|07", hour, msgtime_tm.tm_min, uhandle, msgto, msgb);
							}
						} else {
							snprintf(buffer2, sizeof buffer2, "|08%02d:%02d [|11%s|08->|10%s|08] |02%s|07", msgtime_tm.tm_hour, msgtime_tm.tm_min, uhandle, msgto, msgb);
						}

						append_screenbuffer((unsigned char*)buffer2);
						memset(inputbuffer, 0, 256);
						inputbuffer_at = 0;
						do_update = 1;
                    } else if (_strnicmp(&inputbuffer[1], "nick", 4) == 0 && strlen(inputbuffer) > 6) {
                        raw(usessl, "NICK %s\n", &inputbuffer[6]);
                    } else {
						memset(inputbuffer, 0, 256);
						inputbuffer_at = 0;
						do_update = 1;
                    }
				}
				else if (strlen(inputbuffer) > 0) {
					if (sent_login == true) {
                        raw(usessl, "PRIVMSG %s %s\n", inir.Get("Main", "Room", "#talisman").c_str(), inputbuffer);

						if (ampm) {
							int hour = msgtime_tm.tm_hour;
							if (hour == 0 || hour < 12) {
								if (hour == 0) {
									hour = 12;
								} 
								snprintf(buffer2, sizeof buffer2, "|08%02d:%02dam [|11%s|08]: |07%s", hour, msgtime_tm.tm_min, uhandle, inputbuffer);
							} else {
                                if (hour != 12) {
                                    hour -= 12;
                                }
								snprintf(buffer2, sizeof buffer2, "|08%02d:%02dpm [|11%s|08]: |07%s", hour, msgtime_tm.tm_min, uhandle, inputbuffer);
							}
						} else {
							snprintf(buffer2, sizeof buffer2, "|08%02d:%02d [|11%s|08]: |07%s", msgtime_tm.tm_hour, msgtime_tm.tm_min, uhandle, inputbuffer);
						}
						
                        append_screenbuffer((unsigned char*)buffer2);
					} else {
						snprintf(buffer2, sizeof buffer2, "You are not logged in.");
						append_screenbuffer((unsigned char*)buffer2);
					}
					do_update = 1;
				}
				memset(inputbuffer, 0, 256);
				inputbuffer_at = 0;
			}
			else if (c != '\n' && c != '\0') {
				if (c == '\b' || c == 127) {
					if (inputbuffer_at > 0) {
						inputbuffer_at--;
						inputbuffer[inputbuffer_at] = '\0';
						do_update = 2;
					}
				}
				else if (inputbuffer_at < 255) {
					inputbuffer[inputbuffer_at++] = c;
					do_update = 2;
				}
			}
		}

		if (usessl) {
			ERR_clear_error();
            len = SSL_read(ssl, readbuffer, 511);
        } else {
            len = recv(chat_socket, readbuffer, 511, 0);
        }

		if (len > 0) {
			readbuffer[len] = '\0';
			strncat(partmessage, readbuffer, len);

			while (strchr(partmessage, '\n') != NULL) {
				strncpy(readbuffer, partmessage, sizeof(readbuffer));
				y = 0;
				for (z = 0; z < strlen(readbuffer); z++) {
					if (readbuffer[z] != '\n') {
						message[y] = readbuffer[z];
						message[y + 1] = '\0';
						y++;
					} else {
						break;
					}
				}
				z++;

                char *prefixptr = NULL;
                char *commandptr = NULL;
                char *argsptr = NULL;

                if (message[0] == ':') {
                    prefixptr = message + 1;
                    for (int k = 1; k < strlen(message); k++) {
                        if (message[k] == ' ') {
                            message[k] = '\0';
                            commandptr = &message[k + 1];
                            break;
                        }
                    }
                } else {
                    commandptr = message;
                }

                for (int k = 0; k < strlen(commandptr); k++) {
                    if (commandptr[k] == ' ') {
                        commandptr[k] = '\0';
                        argsptr = &commandptr[k+1];
                        break;
                    }
                }

				msgtime = time(NULL);
#if defined(WIN32) || defined(_MSC_VER)
				localtime_s(&msgtime_tm, &msgtime);
#else
				localtime_r(&msgtime, &msgtime_tm);
#endif

                if (_stricmp(commandptr, "PING") == 0) {
                    raw(usessl, "PONG %s\n", argsptr);
                } else if (_stricmp(commandptr, "JOIN") == 0) {
                    for (int k = 0; k < strlen(prefixptr); k++) {
                        if (prefixptr[k] == '!') {
                            prefixptr[k] = '\0';
                            break;
                        }
                    }
                    if (ampm) {
                        int hour = msgtime_tm.tm_hour;
                        if (hour == 0 || hour < 12) {
                            if (hour == 0) {
                                hour = 12;
                            }
                            snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dam |02>> %s has joined the channel|07", hour, msgtime_tm.tm_min, prefixptr);
                        } else {
                            if (hour != 12) {
                                hour -= 12;
                            }
                            snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dpm |02>> %s has joined the channel|07", hour, msgtime_tm.tm_min, prefixptr);
                        }
                    } else {
                        snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02d |02>> %s has joined the channel|07", msgtime_tm.tm_hour, msgtime_tm.tm_min, prefixptr);
                    }
                    append_screenbuffer((unsigned char*)outputbuffer);
                    do_update = 1;
                } else if (_stricmp(commandptr, "QUIT") == 0 || _stricmp(commandptr, "PART") == 0) {
                    for (int k = 0; k < strlen(prefixptr); k++) {
                        if (prefixptr[k] == '!') {
                            prefixptr[k] = '\0';
                            break;
                        }
                    }
                    if (ampm) {
                        int hour = msgtime_tm.tm_hour;
                        if (hour == 0 || hour < 12) {
                            if (hour == 0) {
                                hour = 12;
                            }
                            snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dam |04<< %s has left the channel|07", hour, msgtime_tm.tm_min, prefixptr);
                        } else {
                            if (hour != 12) {
                                hour -= 12;
                            }
                            snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dpm |04<< %s has left the channel|07", hour, msgtime_tm.tm_min, prefixptr);
                        }
                    } else {
                        snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02d |04<< %s has left the channel|07", msgtime_tm.tm_hour, msgtime_tm.tm_min, prefixptr);
                    }
                    append_screenbuffer((unsigned char*)outputbuffer);
                    do_update = 1;
                } else if (_stricmp(commandptr, "NICK") == 0) {
                    if (prefixptr != NULL) {
                        if (_strnicmp(prefixptr, uhandle, strlen(uhandle)) == 0) {
                            free(uhandle);



                            uhandle = replace_spaces((argsptr[1] == ':' ? &argsptr[1] : argsptr));
                            if (ampm) {
                                int hour = msgtime_tm.tm_hour;
                                if (hour == 0 || hour < 12) {
                                    if (hour == 0) {
                                        hour = 12;
                                    }
                                    snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dam |03>> You are now known as %s|07", hour, msgtime_tm.tm_min, uhandle);
                                } else {
                                    if (hour != 12) {
                                        hour -= 12;
                                    }
                                    snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dpm |03>> You are now known as %s|07", hour, msgtime_tm.tm_min, uhandle);
                                }
                            } else {
                                snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02d |03>> You are now known as %s|07", msgtime_tm.tm_hour, msgtime_tm.tm_min, uhandle);
                            }
                        } else {

                            for (int k = 0; k < strlen(prefixptr); k++) {
                                if (prefixptr[k] == '!') {
                                    prefixptr[k] = '\0';
                                    break;
                                }
                            }
                            if (ampm) {
                                int hour = msgtime_tm.tm_hour;
                                if (hour == 0 || hour < 12) {
                                    if (hour == 0) {
                                        hour = 12;
                                    }
                                    snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dam |13%s is now known as %s|07", hour, msgtime_tm.tm_min, prefixptr, (argsptr[1] == ':' ? &argsptr[1] : argsptr));
                                } else {
                                    if (hour != 12) {
                                        hour -= 12;
                                    }
                                    snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dpm |13%s is now known as %s|07", hour, msgtime_tm.tm_min, prefixptr, (argsptr[1] == ':' ? &argsptr[1] : argsptr));
                                }
                            } else {
                                snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02d |13%s is now known as %s|07", msgtime_tm.tm_hour, msgtime_tm.tm_min, prefixptr, (argsptr[1] == ':' ? &argsptr[1] : argsptr));
                            }
                        }
                        append_screenbuffer((unsigned char*)outputbuffer);

                    }
                    do_update = 1;
                } else if (_stricmp(commandptr, "001") == 0) {
                    if (!sentjoin) {
                        raw(usessl, "JOIN %s\n",inir.Get("Main", "Room", "#talisman").c_str());
                        sentjoin = true;
                    }
                } else if (_stricmp(commandptr, "372") == 0) { // motd
                    char *targetptr = argsptr;
                    char *msgptr = NULL;

                    for (int k = 0; k < strlen(argsptr); k++) {
                        if (targetptr[k] == ' ') {
                            targetptr[k] = '\0';
                            msgptr = &targetptr[k + 2];
                            break;
                        }
                    }
                    if (ampm) {
						int hour = msgtime_tm.tm_hour;
						if (hour == 0 || hour < 12) {
							if (hour == 0) {
								hour = 12;
							}
							snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dam |06%s|07", hour, msgtime_tm.tm_min, msgptr);
						} else {
                            if (hour != 12) {
                                hour -= 12;
                            }
							snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dpm |06%s|07", hour, msgtime_tm.tm_min, msgptr);
						}
					} else {
						snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02d |06%s|07", msgtime_tm.tm_hour, msgtime_tm.tm_min, msgptr);
                    }
                    append_screenbuffer((unsigned char*)outputbuffer);
                    do_update = 1;
                } else if (_stricmp(commandptr, "433") == 0) { // nick in use
                    uhandle = replace_spaces(std::string(std::string(uhandle) + "_").c_str());
                    raw(usessl, "NICK %s\n", uhandle);
                    do_update = 1;
                } else if (_stricmp(commandptr, "353") == 0) {
                    // names response
                    for (int h=0;h < strlen(argsptr);h ++) {
                        if (argsptr[h] == ':') {
                            argsptr = &argsptr[h+1];
                            break;
                        }
                    }
                    if (ampm) {
						int hour = msgtime_tm.tm_hour;
						if (hour == 0 || hour < 12) {
							if (hour == 0) {
								hour = 12;
							}
							snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dam |11NAMES: |09%s|07", hour, msgtime_tm.tm_min, argsptr);
						} else {
                            if (hour != 12) {
                                hour -= 12;
                            }
							snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dpm |11NAMES: |09%s|07", hour, msgtime_tm.tm_min, argsptr);
						}
					} else {
						snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02d |11NAMES: |09%s|07", msgtime_tm.tm_hour, msgtime_tm.tm_min, argsptr);
                    }
                    append_screenbuffer((unsigned char*)outputbuffer);
                    do_update = 1;

                } else if (_stricmp(commandptr, "PRIVMSG") == 0) {
                    char *targetptr = argsptr;
                    char *msgptr = NULL;
                    char *fromptr = prefixptr;
                    bool action = false;
                    for (int k = 0; k < strlen(prefixptr); k++) {
                        if (prefixptr[k] == '!') {
                            prefixptr[k] = '\0';
                            break;
                        }
                    }

                    for (int k = 0; k < strlen(argsptr); k++) {
                        if (targetptr[k] == ' ') {
                            targetptr[k] = '\0';
                            msgptr = &targetptr[k + 2];
                            break;
                        }
                    }

                    if (msgptr[0] == 1 && _strnicmp(&msgptr[1], "ACTION", 6) == 0){
                        action = true;
                        msgptr = &msgptr[8];
                        msgptr[strlen(msgptr) - 1] = '\0';
                    } else {
                        action = false;
                    }
                    if (_stricmp(targetptr, inir.Get("Main", "Room", "#talisman").c_str()) == 0) {
						if (ampm) {
							int hour = msgtime_tm.tm_hour;
							if (hour == 0 || hour < 12) {
								if (hour == 0) {
									hour = 12;
								}
								if (strstr(msgptr, uhandle) != NULL) {
                                    if (action) {
                                        snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dam |15*|11%s |15%s|07", hour, msgtime_tm.tm_min, fromptr, msgptr);
                                    } else {
                                        snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dam [|11%s|08]: |15%s|07", hour, msgtime_tm.tm_min, fromptr, msgptr);
                                    }
                                } else {
                                    if (action) {
                                        snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dam |07*|11%s |07%s", hour, msgtime_tm.tm_min, fromptr, msgptr);
                                    } else {
                                        snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dam [|11%s|08]: |07%s", hour, msgtime_tm.tm_min, fromptr, msgptr);
                                    }
                                }
							} else {
                                if (hour != 12) {
                                    hour -= 12;
                                }
								if (strstr(msgptr, uhandle) != NULL) {
                                    if (action) {
                                        snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dpm |15*|11%s |15%s|07", hour, msgtime_tm.tm_min, fromptr, msgptr);
                                    } else {
                                        snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dpm [|11%s|08]: |15%s|07", hour, msgtime_tm.tm_min, fromptr, msgptr);
                                    }
                                } else {
                                    if (action) {
                                        snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dpm |07*|11%s |07%s", hour, msgtime_tm.tm_min, fromptr, msgptr);
                                    } else {
                                        snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dpm [|11%s|08]: |07%s", hour, msgtime_tm.tm_min, fromptr, msgptr);
                                    }
                                }
							}
						} else {
                            if (strstr(msgptr, uhandle) != NULL) {
                                if (action) {
                                    snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02d |15*|11%s |15%s|07", msgtime_tm.tm_hour, msgtime_tm.tm_min, fromptr, msgptr);
                                } else {
                                    snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02d [|11%s|08]: |15%s|07", msgtime_tm.tm_hour, msgtime_tm.tm_min, fromptr, msgptr);
                                }
                            } else {
                                if (action) {
                                    snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02d |07*|11%s |07%s", msgtime_tm.tm_hour, msgtime_tm.tm_min, fromptr, msgptr);
                                } else {
                                    snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02d [|11%s|08]: |07%s", msgtime_tm.tm_hour, msgtime_tm.tm_min, fromptr, msgptr);
                                }
                            }
						}
                    } else {
						if (ampm) {
							int hour = msgtime_tm.tm_hour;
							if (hour == 0 || hour < 12) {
								if (hour == 0) {
									hour = 12;
								}
								if (action) {
                                    snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dam [|11%s|08->|10%s|08] |10*|11%s |10%s", hour, msgtime_tm.tm_min, fromptr, fromptr, uhandle, msgptr);
                                } else {
                                    snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dam [|11%s|08->|10%s|08] |10%s", hour, msgtime_tm.tm_min, fromptr, uhandle, msgptr);
                                }
							} else {
                                if (hour != 12) {
                                    hour -= 12;
                                }
                                if (action) {
                                    snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dpm [|11%s|08->|10%s|08] |10*|11%s |10%s", hour, msgtime_tm.tm_min, fromptr, fromptr, uhandle, msgptr);
                                } else {
                                    snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dpm [|11%s|08->|10%s|08] |10%s", hour, msgtime_tm.tm_min, fromptr, uhandle, msgptr);
                                }
							}
						} else {
                            if (action) {
                                snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02d [|11%s|08->|10%s|08] |10*|11%s |10%s", msgtime_tm.tm_hour, msgtime_tm.tm_min, fromptr, fromptr, uhandle, msgptr);
                            } else {
                                snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02d [|11%s|08->|10%s|08] |10%s", msgtime_tm.tm_hour, msgtime_tm.tm_min, fromptr, uhandle, msgptr);
                            }
						}
                    }
                    append_screenbuffer((unsigned char*)outputbuffer);
                    do_update = 1;
                } else {

					if (ampm) {
						int hour = msgtime_tm.tm_hour;
						if (hour == 0 || hour < 12) {
							if (hour == 0) {
								hour = 12;
							}
							snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dam |03%s %s|07", hour, msgtime_tm.tm_min, commandptr, argsptr);
						} else {
                            if (hour != 12) {
                                hour -= 12;
                            }
							snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02dpm |03%s %s|07", hour, msgtime_tm.tm_min, commandptr, argsptr);
						}
					} else {
						snprintf(outputbuffer, sizeof outputbuffer, "|08%02d:%02d |03%s %s|07", msgtime_tm.tm_hour, msgtime_tm.tm_min, commandptr, argsptr);
                    }

                    // screen_append output buffer
                    append_screenbuffer((unsigned char*)outputbuffer);
                    do_update = 1;
                }
                if (z < strlen(readbuffer)) {
                    memset(partmessage, 0, 1024);
                    memcpy(partmessage, &readbuffer[z], strlen(readbuffer) - z);
                } else {
                    memset(partmessage, 0, 1024);
                }

			} 
		}
		else {
            if (usessl) {
                int err = SSL_get_error(ssl, len);
                if (err == SSL_ERROR_ZERO_RETURN || err == SSL_ERROR_SYSCALL) {
                    SSL_shutdown(ssl);
                    SSL_free(ssl);
                    SSL_CTX_free(ctx);
#if defined(_MSC_VER) || defined(WIN32)
                    closesocket(chat_socket);
#else
                    close(chat_socket);
#endif
                    for (i = 0; i < 22; i++) {
                        free(screenbuffer[i]);
                    }
                    free(screenbuffer);
                    return;
                }
            } else {
                if (errno != EAGAIN && errno != EWOULDBLOCK) {
#if defined(_MSC_VER) || defined(WIN32)
                    closesocket(chat_socket);
#else
                    close(chat_socket);
#endif
                    for (i = 0; i < 22; i++) {
                        free(screenbuffer[i]);
                    }
                    free(screenbuffer);
                    return;
                }
            }
		}

		if (!sent_login) {

            if (usessl) {
                secure = SSL_get_verify_result(ssl);

                raw(usessl, "USER %s 0 0 :%s\n", uhandle, od_control_get()->user_name);
				raw(usessl, "NICK %s\n", uhandle);
				sent_login = true;
				connected_at = time(NULL);

				if (secure != 0) {
					snprintf(buffer2, sizeof buffer2, "|14There was an SSL error connecting, see /cert for details");
					append_screenbuffer((unsigned char*)buffer2);
					do_update = 1;
				}
            } else {
                raw(usessl, "USER %s 0 0 :%s\n", uhandle, od_control_get()->user_name);
                raw(usessl, "NICK %s\n", uhandle);
                sent_login = true;
                connected_at = time(NULL);
            }

		}

		if (do_update == 1) {
			od_clr_scr();
			for (i = 0; i <= line_at; i++) {
				for (z = 0; z < 80; z++) {
					if (screenbuffer[i][z]->color != last_color) {
						switch (screenbuffer[i][z]->color) {
						case 0:
							od_printf("`black`");
							break;
						case 1:
							od_printf("`blue`");
							break;
						case 2:
							od_printf("`green`");
							break;
						case 3:
							od_printf("`cyan`");
							break;
						case 4:
							od_printf("`red`");
							break;
						case 5:
							od_printf("`magenta`");
							break;
						case 6:
							od_printf("`yellow`");
							break;
						case 7:
							od_printf("`white`");
							break;
						case 8:
							od_printf("`bright black`");
							break;
						case 9:
							od_printf("`bright blue`");
							break;
						case 10:
							od_printf("`bright green`");
							break;
						case 11:
							od_printf("`bright cyan`");
							break;
						case 12:
							od_printf("`bright red`");
							break;
						case 13:
							od_printf("`bright magenta`");
							break;
						case 14:
							od_printf("`bright yellow`");
							break;
						case 15:
							od_printf("`bright white`");
							break;
						}
						last_color = screenbuffer[i][z]->color;
					}
					if (screenbuffer[i][z]->c == '\0') {
						break;
					}
					else {
                        if (screenbuffer[i][z]->c == '`') {
                            od_control.od_color_delimiter='~';
                            od_printf("`");
                            od_control.od_color_delimiter='`';
                        } else {
                            od_printf("%c", screenbuffer[i][z]->c);
                        }
					}
				}
				od_printf("\r\n");
			}
			for (i = line_at + 1; i < 22; i++) {
				od_printf("\r\n");
			}
			od_set_cursor(23, 1);
			od_printf("`bright black`-------------------------------------------------------------------------------");
			od_set_cursor(23, 1);
            if (!usessl) {
                od_printf("`bright black`[`bright cyan`%s`bright black`][%s]", uhandle, SSL_str[2]);
            } else {
                od_printf("`bright black`[`bright cyan`%s`bright black`][%s]", uhandle, (secure == 0 ? SSL_str[1]: SSL_str[0]));
            }
			od_set_cursor(24, 1);
			od_printf("`bright black`[`bright green`%s`bright black`]", inir.Get("Main", "Room", "#talisman").c_str());
			od_set_color(D_GREY, D_BLACK);
			od_set_cursor(23, 57);
			if (strlen(inputbuffer) > 79 - (strlen(inir.Get("Main", "Room", "#talisman").c_str()) + 4)) {
				od_set_cursor(24, strlen(inir.Get("Main", "Room", "#talisman").c_str()) + 3);
				od_printf("<%s", &inputbuffer[strlen(inputbuffer) - (78 - (strlen(inir.Get("Main", "Room", "#talisman").c_str()) + 4))]);
				od_clr_line();
			}
			else {
				od_set_cursor(24, strlen(inir.Get("Main", "Room", "#talisman").c_str()) + 4);
				od_printf("%s", inputbuffer);
				od_clr_line();
			}
			do_update = 0;
		}
		else if (do_update == 2) {
			od_set_cursor(23, 57);
			if (strlen(inputbuffer) > 79 - (strlen(inir.Get("Main", "Room", "#talisman").c_str()) + 4)) {
				od_set_cursor(24, strlen(inir.Get("Main", "Room", "#talisman").c_str()) + 4);
				od_printf("<%s", &inputbuffer[strlen(inputbuffer) - (78 - (strlen(inir.Get("Main", "Room", "#talisman").c_str()) + 4))]);
				od_clr_line();
			}
			else {
				od_set_cursor(24, strlen(inir.Get("Main", "Room", "#talisman").c_str()) + 4);
				od_printf("%s", inputbuffer);
				od_clr_line();
			}
		}
	}
}

#if defined(_MSC_VER) || defined(WIN32)
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nCmdShow)
{

	WORD wVersionRequested;
	WSADATA wsaData;
	int err;

	wVersionRequested = MAKEWORD(2, 2);

	err = WSAStartup(wVersionRequested, &wsaData);

	od_parse_cmd_line(lpszCmdLine);
#else
int main(int argc, char** argv)
{
	od_parse_cmd_line(argc, argv);
#endif

	od_init();

	Program *p = new Program();

	p->run();
	
	od_clr_scr();

	od_exit(0, FALSE);

	return 0;
}
